package main

import "net/http"

const html = `
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" type="image/png" href="https://cdn.bulldog.dev/files/levi/goham.png" />
	<title>k8s l2i dev</title>
	<link rel="stylesheet" href="https://cdn.bulldog.dev/lib/bootstrap/4.3.1/bootstrap.min.css">
</head>
<body>
	<div class="container">
        <div class="row">
            <div class="col">
                <h1>Hi</h1>
	            <p><img class="img-fluid" src="https://cdn.bulldog.dev/files/levi/goham.png" alt="Golang gopher with chess pieces" /></p>
            </div>
        </div>
    </div>
</body>
</html>
`

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Server", "levik8s")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Write([]byte(html))
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":80", nil)
}
